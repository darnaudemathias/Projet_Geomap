﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace GeoMap
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MenuCours : Page
    {
        public MenuCours()
        {
            this.InitializeComponent();
            List<product> p1 = new List<product>();
            p1.Add(new product() { name = "Albanie", ImagePath = "/Assets/Drapeau/Albanie.png" });
            p1.Add(new product() { name = "Allemagne", ImagePath = "/Assets/Drapeau/Allemagne.png" });
            p1.Add(new product() { name = "Arménie", ImagePath = "/Assets/Drapeau/Arménie.png" });
            p1.Add(new product() { name = "Autriche", ImagePath = "/Assets/Drapeau/Autriche.png" });
            p1.Add(new product() { name = "Azerbaïdjan", ImagePath = "/Assets/Drapeau/Azerbaïdjan.png" });
            p1.Add(new product() { name = "Belgique", ImagePath = "/Assets/Drapeau/Belgique.png" });
            p1.Add(new product() { name = "Biélorussie", ImagePath = "/Assets/Drapeau/Biélorussie.png" });
            p1.Add(new product() { name = "Bosnie-et-Herzégovine", ImagePath = "/Assets/Drapeau/Bosnie-et-Herzégovine.png" });
            p1.Add(new product() { name = "Bulgarie", ImagePath = "/Assets/Drapeau/Bulgarie.png" });
            p1.Add(new product() { name = "Croatie", ImagePath = "/Assets/Drapeau/Croatie.png" });
            p1.Add(new product() { name = "Danemark", ImagePath = "/Assets/Drapeau/Danemark.png" });
            p1.Add(new product() { name = "Espagne", ImagePath = "/Assets/Drapeau/Espagne.png" });
            p1.Add(new product() { name = "Estonie", ImagePath = "/Assets/Drapeau/Estonie.png" });
            p1.Add(new product() { name = "Finlande", ImagePath = "/Assets/Drapeau/Finlande.png" });
            p1.Add(new product() { name = "France", ImagePath = "/Assets/Drapeau/France.png" });
            p1.Add(new product() { name = "Géorgie", ImagePath = "/Assets/Drapeau/Géorgie.png" });
            p1.Add(new product() { name = "Grèce", ImagePath = "/Assets/Drapeau/Grèce.png" });
            p1.Add(new product() { name = "Hongrie", ImagePath = "/Assets/Drapeau/Hongrie.png" });
            p1.Add(new product() { name = "Irlande", ImagePath = "/Assets/Drapeau/Irlande.png" });
            p1.Add(new product() { name = "Islande", ImagePath = "/Assets/Drapeau/Islande.png" });
            p1.Add(new product() { name = "Italie", ImagePath = "/Assets/Drapeau/Italie.png" });
            p1.Add(new product() { name = "Kazakhstan", ImagePath = "/Assets/Drapeau/Kazakhstan.png" });
            p1.Add(new product() { name = "Lettonie", ImagePath = "/Assets/Drapeau/Lettonie.png" });
            p1.Add(new product() { name = "Lituanie", ImagePath = "/Assets/Drapeau/Lituanie.png" });
            p1.Add(new product() { name = "Luxembourg", ImagePath = "/Assets/Drapeau/Luxembourg.png" });
            p1.Add(new product() { name = "Macédoine", ImagePath = "/Assets/Drapeau/Macédoine.png" });
            p1.Add(new product() { name = "Moldavie", ImagePath = "/Assets/Drapeau/Moldavie.png" });
            p1.Add(new product() { name = "Monténégro", ImagePath = "/Assets/Drapeau/Monténégro.png" });
            p1.Add(new product() { name = "Norvège", ImagePath = "/Assets/Drapeau/Norvège.png" });
            p1.Add(new product() { name = "Pays-Bas", ImagePath = "/Assets/Drapeau/Pays-Bas.png" });
            p1.Add(new product() { name = "Pologne", ImagePath = "/Assets/Drapeau/Pologne.png" });
            p1.Add(new product() { name = "Portugal", ImagePath = "/Assets/Drapeau/Portugal.png" });
            p1.Add(new product() { name = "Roumanie", ImagePath = "/Assets/Drapeau/Roumanie.png" });
            p1.Add(new product() { name = "Royaume-Uni", ImagePath = "/Assets/Drapeau/Royaume-Unie.png" });
            p1.Add(new product() { name = "Russie", ImagePath = "/Assets/Drapeau/Russie.png" });
            p1.Add(new product() { name = "Serbie", ImagePath = "/Assets/Drapeau/Serbie.png" });
            p1.Add(new product() { name = "Slovaquie", ImagePath = "/Assets/Drapeau/Slovaquie.png" });
            p1.Add(new product() { name = "Suède", ImagePath = "/Assets/Drapeau/Suède.png" });
            p1.Add(new product() { name = "Suisse", ImagePath = "/Assets/Drapeau/Suisse.png" });
            p1.Add(new product() { name = "Tchéquie", ImagePath = "/Assets/Drapeau/Tchéquie.png" });
            p1.Add(new product() { name = "Turquie", ImagePath = "/Assets/Drapeau/Turquie.png" });
            p1.Add(new product() { name = "Ukraine", ImagePath = "/Assets/Drapeau/Ukraine.png" });

            ListePays.DataContext = p1;
        }
        

        private void Button_click(object sender, RoutedEventArgs e)
        {            
            Frame.Navigate(typeof(Menu));
        }
    }
  

}
