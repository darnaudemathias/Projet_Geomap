﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace GeoMap
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MenuQuiz : Page
    {
        int Score = 0;
        public MenuQuiz()
        {
            this.InitializeComponent();           
        }          

        private void TextBox1_TextChanged(object sender, TextChangedEventArgs e)
        {            
            if (TextBox1.Text == "france" || TextBox1.Text == "France" )
            {
                Correct1.Visibility = Visibility.Visible;
                TextBox1.IsEnabled = false;
                Score = Score + 1;
            }            
        }

        private void TextBox2_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBox2.Text == "allemagne" || TextBox2.Text == "Allemagne")
            {
                Correct2.Visibility = Visibility.Visible;
                TextBox2.IsEnabled = false;
                Score = Score + 1;
            }            
        }

        private void TextBox3_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBox3.Text == "suisse" || TextBox3.Text == "Suisse")
            {
                Correct3.Visibility = Visibility.Visible;
                TextBox3.IsEnabled = false;
                Score = Score + 1;
            }            
        }

        private void TextBox4_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBox4.Text == "danemark" || TextBox4.Text == "Danemark")
            {
                Correct4.Visibility = Visibility.Visible;
                TextBox4.IsEnabled = false;
                Score = Score + 1;
            }            
        }

        private void TextBox5_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBox5.Text == "espagne" || TextBox5.Text == "Espagne")
            {
                Correct5.Visibility = Visibility.Visible;
                TextBox5.IsEnabled = false;
                Score = Score + 1;
            }            
        }

        private void TextBox6_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBox6.Text == "italie" || TextBox6.Text == "Italie")
            {
                Correct6.Visibility = Visibility.Visible;
                TextBox6.IsEnabled = false;
                Score = Score + 1;
            }
            
        }

        private void TextBox7_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBox7.Text == "finlande" || TextBox7.Text == "Finlande")
            {
                Correct7.Visibility = Visibility.Visible;
                TextBox7.IsEnabled = false;
                Score = Score + 1;
            }            
        }

        private void TextBox8_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBox8.Text == "russie" || TextBox8.Text == "Russie")
            {
                Correct8.Visibility = Visibility.Visible;
                TextBox8.IsEnabled = false;
                Score = Score + 1;
            }         
        }

        private void TextBox9_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBox9.Text == "suède" || TextBox9.Text == "Suède")
            {
                Correct9.Visibility = Visibility.Visible;
                TextBox9.IsEnabled = false;
                Score = Score + 1;
            }            
        }

        private void TextBox10_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBox10.Text == "irlande" || TextBox10.Text == "Irlande")
            {
                Correct10.Visibility = Visibility.Visible;
                TextBox10.IsEnabled = false;
                Score = Score + 1;
            }            
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ContentDialog monDialog = new ContentDialog()
            {
                Title = " Score",
                Content = "Ton score est de : " + Score + "sur 10.",
                PrimaryButtonText = "Ok"
            };
            ContentDialogResult result = await monDialog.ShowAsync();
            Frame.Navigate(typeof(Menu));
        }
        
    }
}
